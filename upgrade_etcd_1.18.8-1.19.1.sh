#!/bin/bash
set -x
REGISTRY=registry.rdleas.ru:5000/k8s.gcr.io/etcd
# For each machine
ETCD_VERSION=3.4.13-0
TOKEN=SblEsbRndTocken01
CLUSTER_STATE=new
NAME_1=etcd01
NAME_2=etcd02
NAME_3=etcd03
HOST_1=10.77.193.1
HOST_2=10.77.193.2
HOST_3=10.77.193.3
CLUSTER=${NAME_1}=http://${HOST_1}:2380,${NAME_2}=http://${HOST_2}:2380,${NAME_3}=http://${HOST_3}:2380
DATA_DIR=/storage1/etcd
# For current node
THIS_NAME=$1
THIS_IP=$2
docker run -d \
  --restart always \
  -p 4001:4001 \
  -p 2379:2379 \
  -p 2380:2380 \
  --volume=/etc/ssl/certs:/etc/ssl/certs \
  --volume=${DATA_DIR}:/var/lib/etcd \
  --name etcd-${ETCD_VERSION} ${REGISTRY}:${ETCD_VERSION} \
  etcd \
  --name ${THIS_NAME} \
  --initial-advertise-peer-urls http://${THIS_IP}:2380 \
  --listen-peer-urls http://0.0.0.0:2380 \
  --advertise-client-urls=http://${THIS_IP}:2379,http://${THIS_IP}:4001 \
  --listen-client-urls http://0.0.0.0:2379,http://0.0.0.0:4001 \
  --initial-cluster ${CLUSTER} \
  --initial-cluster-state ${CLUSTER_STATE} \
  --initial-cluster-token ${TOKEN} \
  --data-dir=/var/lib/etcd \
  --auto-tls --peer-auto-tls
