# This is the provider used to spin up the gcloud instance
provider "google-beta" {
  project = var.project_name
  region  = var.region_name
  zone    = var.zone_name
  access_token = data.external.access_token.result.token
}

resource "google_container_cluster" "gke-cluster" {
  provider = google-beta
  name     = var.gke_cluster_name
  location = var.zone_name

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  addons_config {
    istio_config  {
        disabled = false
    }
  }
}

resource "google_container_node_pool" "gke-cluster-nodes" {
  provider = google-beta
  name       = "${var.gke_cluster_name}-node-pool"
  location   = var.zone_name
  cluster    = google_container_cluster.gke-cluster.name
  node_count = var.cluster_node_count

  node_config {
    preemptible  = true
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

# Получаем токен для доступа к GCP
data "external" "access_token" {
  program = ["./get_token.sh"]
}

# Получаем данные для доступа к кластеру.
resource "null_resource" "provision" {
  depends_on  =   [google_container_cluster.gke-cluster]

  provisioner "local-exec" {
    command     = "KUBECONFIG=./kubeconfig gcloud container clusters get-credentials ${var.gke_cluster_name} --zone ${var.zone_name} --project ${var.project_name}"
    working_dir = path.module
  }
}
