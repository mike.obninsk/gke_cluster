#!/bin/bash
#  kubectl drain NODENAME --ignore-daemonsets --delete-local-data
  sudo apt-mark unhold kubernetes-cni kubeadm
  sudo apt-get update
  sudo apt-get install -y kubeadm=1.19.1-00
  sudo kubeadm upgrade node
  sudo apt-mark unhold kubelet kubectl
  sudo apt-get install -y kubelet=1.19.1-00 kubectl=1.19.1-00
  sudo apt-mark hold kube*
  sudo apt-get update && sudo apt-get upgrade -y
  sudo reboot
#  kubectl uncordon NODENAME
